package com.google.eclipse.protobuf;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.eclipse.protobuf.proto.Protobuf;
import com.google.inject.Injector;

public class ProtobufLoader {

	public static Protobuf loadFromFile(String path) {
		
		Injector injector = new ProtobufStandaloneSetup().createInjectorAndDoEMFRegistration();
		XtextResourceSet resourceSet = injector.getInstance(XtextResourceSet.class);					
		Resource resource = resourceSet.getResource(URI.createFileURI(path), true);
		
		return (Protobuf) resource.getContents().get(0);
		
	}
	
}
