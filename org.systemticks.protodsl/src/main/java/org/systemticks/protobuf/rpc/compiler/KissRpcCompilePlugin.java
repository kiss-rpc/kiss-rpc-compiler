package org.systemticks.protobuf.rpc.compiler;

import org.gradle.api.Plugin;
import org.gradle.api.Project;

public class KissRpcCompilePlugin implements Plugin<Project> {

	static final String TASK_NAME = "kissRpcCompile";

	@Override
	public void apply(Project target) {
		
		KissRpcCompilePluginExtension ext = target.getExtensions().create("kissrpc", KissRpcCompilePluginExtension.class);
				
		KissRpcCompileTask task = target.getTasks().create(TASK_NAME, KissRpcCompileTask.class);
	}

}
