package org.systemticks.protobuf.rpc.compiler.internal

import com.google.eclipse.protobuf.proto.ComplexTypeLink
import com.google.eclipse.protobuf.proto.MapTypeLink
import com.google.eclipse.protobuf.proto.MessageField
import com.google.eclipse.protobuf.proto.MessageLink
import com.google.eclipse.protobuf.proto.ModifierEnum
import com.google.eclipse.protobuf.proto.Package
import com.google.eclipse.protobuf.proto.Protobuf
import com.google.eclipse.protobuf.proto.Rpc
import com.google.eclipse.protobuf.proto.ScalarType
import com.google.eclipse.protobuf.proto.ScalarTypeLink
import com.google.eclipse.protobuf.proto.Service
import com.google.eclipse.protobuf.proto.TypeLink

class ProxyAndStubGenerator {

//	def writeToFile(String path, Protobuf model) {
//		
//		val service = model.elements.filter(typeof(Service)).head.name
//		val completePath = path + '/' + model.packageName.replaceAll('\\.', '/') + "/"
//		
//		val destination = new File(completePath)
//		if(!destination.exists) {
//			destination.mkdirs
//		}		
//				
//		val fwProxy = new FileWriter(completePath+service+'Proxy.java')				
//		fwProxy.write(model.generateEntireProxy.toString)
//		fwProxy.close
//
//		val fwStub = new FileWriter(completePath+service+'Stub.java')
//		fwStub.write(model.generateEntireStub.toString)
//		fwStub.close
//				
//	}

	def generateEntireProxy(Protobuf model) {
	'''
			�model.generatePackageAndImports�
			
			�FOR service: model.elements.filter(typeof(Service))�	
				public abstract class �service.name�Proxy extends RpcServiceProxy {
					
					//Constructor
					�service.generateProxyConstructor�
					
					//all requests and reponses
					�FOR method: service.elements.filter(typeof(Rpc))�
						�method.generateProxyRequest�
						
						�method.generateProxyResponse�
						
					�ENDFOR�
					
					//dispatcher for incoming responses
					�service.generateProxyDispatcher�
				}
			�ENDFOR�
		'''	
	}

	def generatePackageAndImports(Protobuf model) {
	'''
			package �model.packageName�;
			
			import org.systemticks.protobuf.rpc.data.Header.MessageType;
			import org.systemticks.protobuf.rpc.data.*;
			import com.google.protobuf.InvalidProtocolBufferException;
			import org.systemticks.structuredlogging.*;
			
			import java.util.List;
			import java.util.Map;
			
			import �model.packageName�.*;
		'''	
	}

	def generateProxyConstructor(Service service) {
	'''
			public �service.name�Proxy(String proxyId) {
				super("�service.name�", proxyId);
			}
		'''	
	}

	def generateProxyDispatcher(Service service) {
	'''
			@Override
			public synchronized void dispatchMessage(MsgQueueElement msg) throws InvalidProtocolBufferException {
				
				SLog.logMethodEnter("�service.name�Proxy", "dispatchMessage", SLog.createParams("header", msg.getHeader()), null);
						
				if(msg.getHeader().getType().equals(MessageType.RESPONSE)) {
					�FOR method: service.elements.filter(typeof(Rpc))�					
						if(msg.getHeader().getMethod().equals("�method.name�")) {
								�method.returnType.target.name� obj = �method.returnType.target.name�.parseFrom(msg.getMessage());
								�method.reponse�(�method.returnType.messageFields.map[getter+'()'].join(', ')�);
						}
					�ENDFOR�
				}
			
			
			}
		'''		
	}

	def generateProxyRequest(Rpc rpc) {
	'''
			public void �rpc.request� ( �rpc.argType.toParams� ) {
			
				SLog.logMethodEnter("�rpc.service.name�Proxy", "�rpc.request�", SLog.createParams(�rpc.argType.toLogParams�), null);				
			
				byte[] payload = �rpc.argType.target.name�.newBuilder()
					�FOR field: rpc.argType.messageFields�
						.�field.setter��field.name.toFirstUpper�(�field.name�)
					�ENDFOR�
					.build().toByteArray();
			
				enqueueNewMessage("�rpc.name�", MessageType.REQUEST, getToken(), payload);
				
			}
		'''
	}
	
	def generateProxyResponse(Rpc rpc) {
	'''
			public abstract void �rpc.reponse� ( �rpc.returnType.toParams� );		
		'''
	}
		

	def generateEntireStub(Protobuf model) {
	'''
			�model.generatePackageAndImports�
			
			�FOR service: model.elements.filter(typeof(Service))�	
				public abstract class �service.name�Stub extends RpcServiceInterface {
					
					//Constructor
					�service.generateStubConstructor�
					
					//all requests and reponses
					�FOR method: service.elements.filter(typeof(Rpc))�
						�method.generateStubResponse�
						
						�method.generateStubRequest�
						
					�ENDFOR�
					
					//dispatcher for incoming requests
					�service.generateStubDispatcher�
				}
			�ENDFOR�
		'''	
	}

	def generateStubConstructor(Service service) {
	'''
			private String currentClientToken = "";
			
			public �service.name�Stub() {
				super("�service.name�");
			}
		'''	
	}


	def generateStubDispatcher(Service service) {
	'''
			@Override
			public synchronized void dispatchMessage(MsgQueueElement msg) throws InvalidProtocolBufferException {
				
				SLog.logMethodEnter("�service.name�Stub", "dispatchMessage", SLog.createParams("header", msg.getHeader()), null);
								
				if(msg.getHeader().getType().equals(MessageType.REQUEST)) {
					currentClientToken = msg.getHeader().getClientToken();
					�FOR method: service.elements.filter(typeof(Rpc))�					
						if(msg.getHeader().getMethod().equals("�method.name�")) {
							�method.argType.target.name� obj = �method.argType.target.name�.parseFrom(msg.getMessage());
							�method.request�(�method.argType.messageFields.map[getter+'()'].join(', ')�);
						}
					�ENDFOR�
				}
				
			}
		'''		
	}

	def generateStubResponse(Rpc rpc) {
	'''
			public void �rpc.reponse� ( �rpc.returnType.toParams� ) {
			
				SLog.logMethodEnter("�rpc.service.name�Stub", "�rpc.reponse�", SLog.createParams(�rpc.returnType.toLogParams�), null);				
			
				byte[] payload = �rpc.returnType.target.name�.newBuilder()
					�FOR field: rpc.returnType.messageFields�
						.�field.setter��field.name.toFirstUpper�(�field.name�)
					�ENDFOR�
					.build().toByteArray();
			
				enqueueNewMessage("�rpc.name�", MessageType.RESPONSE, currentClientToken, payload);
				
			}
		'''
	}
	
	def generateStubRequest(Rpc rpc) {
	'''
			public abstract void �rpc.request� ( �rpc.argType.toParams� );		
		'''
	}

	
	// Helper and transformer
	def getProtoPath(Protobuf model) {
		model.packageName.replaceAll('\\.', '/') + "/"		
	}
	
	def getServices(Protobuf model) {
		model.elements.filter(typeof(Service))		
	}
	
	
	def getRpcs(Service service) {
		service.elements.filter(typeof(Rpc))
	}

	def getService(Rpc rpc) {
		rpc.eContainer as Service
	}

	def getMessageFields(MessageLink link) {
		link.target.elements.filter(typeof(MessageField))		
	}

	
	def getPackageName(Protobuf model) {
		model.elements.filter(typeof(Package)).head.importedNamespace		
	}

	def setter(MessageField field) {
		switch field {
			case field.isList : 'addAll'
			case field.type instanceof MapTypeLink : 'putAll'
			default : 'set'
		}
	}

	def getter(MessageField field) {
		switch field {
			case field.isList : 'obj.get'+field.name.toFirstUpper+'List'
			case field.type instanceof MapTypeLink : 'obj.get'+field.name.toFirstUpper+'Map'
			default : 'obj.get'+field.name.toFirstUpper
		}
	}


	def request(Rpc rpc) {
		rpc.name+"Request"
	}

	def reponse(Rpc rpc) {
		rpc.name+"Response"
	}


	def toParams(MessageLink link) {
		link.messageFields.map[toType+' '+name].join(", ")
	}

	def toLogParams(MessageLink link) {
		link.messageFields.map['"'+name+'", '+name].join(", ")
	}
	
	def isList(MessageField field) {
		field?.modifier.equals(ModifierEnum.REPEATED)
	}

	def toType(MessageField field) {
		if(field.list) "List<"+field.type.toJavaType+">" else field.type.toJavaType
	}
	
	dispatch def String toJavaType(TypeLink typeLink) {  "to do" }
	
	dispatch def String toJavaType(ScalarTypeLink typeLink) { 
		typeLink.target.toJavaPrimitive
	}
	
	dispatch def String toJavaType(ComplexTypeLink typeLink) { 
		typeLink.target.name
	}

	dispatch def String toJavaType(MapTypeLink typeLink) { 
		"Map<"+typeLink.target.keyType.toJavaType+", "+typeLink.target.valueType.toJavaType+'>'
	}
	
	def toJavaPrimitive(ScalarType scalar) {
		switch  scalar {
			case BOOL: 'Boolean'
			case DOUBLE : 'Double'
			case FLOAT : 'Float'
			case INT32 : 'Integer'
			case INT64 : 'Long'
			case UINT32 : 'Integer'
			case UINT64 : 'Long'
			case STRING : 'String'
			default : 'not mapped'
		}
	}
		
}
