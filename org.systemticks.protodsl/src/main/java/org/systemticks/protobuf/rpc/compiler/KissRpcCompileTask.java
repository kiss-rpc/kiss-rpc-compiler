package org.systemticks.protobuf.rpc.compiler;

import java.io.File;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.TaskAction;
import org.systemticks.protobuf.rpc.compiler.internal.KissRpcCompiler;

public class KissRpcCompileTask extends DefaultTask {

	private String sourceDir = "./src/test/resources";
	private String destDir = "./generated";
	
	@Input
	public String getSourceDir() {
		return sourceDir;
	}

	public void setSourceDir(String sourceDir) {
		this.sourceDir = sourceDir;
	}

	@Input
	public String getDestDir() {
		return destDir;
	}

	public void setDestDir(String destDir) {
		this.destDir = destDir;
	}
	
	@TaskAction
	public void compile() throws Exception {
		
		KissRpcCompilePluginExtension ext = getProject().getExtensions().findByType(KissRpcCompilePluginExtension.class);
		if(ext != null) {
			sourceDir = ext.getSourcePath();
			destDir = ext.getDestinationPath();			
		}
		
		System.out.println("Now compiling... ");
		System.out.println("Source Dir = "+sourceDir);
		System.out.println("Dest Dir   = "+destDir);
		
		KissRpcCompiler compiler = new KissRpcCompiler(new File(sourceDir), new File(destDir));
		
		compiler.doGenerate();

	}
	
}
