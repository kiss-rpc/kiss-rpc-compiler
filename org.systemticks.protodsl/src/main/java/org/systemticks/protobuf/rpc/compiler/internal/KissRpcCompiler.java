package org.systemticks.protobuf.rpc.compiler.internal;

import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.IOException;

import com.google.eclipse.protobuf.ProtobufLoader;
import com.google.eclipse.protobuf.proto.Protobuf;

public class KissRpcCompiler {

	File srcPath;
	File destPath;
	
	public KissRpcCompiler(File srcPath, File destPath) {
		super();
		this.srcPath = srcPath;
		this.destPath = destPath;
	}		
	
	public boolean doGenerate() {
				
		ProxyAndStubGenerator generator = new ProxyAndStubGenerator();
		
		if(!srcPath.exists()) {
			return false;
		}
		
		if(!destPath.exists()) {
			destPath.mkdirs();
		}
		
		File[] protoFiles = srcPath.listFiles(new FileFilter() {			
			@Override
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().endsWith("proto");
			}
		});
		
		for(File f: protoFiles) {
			Protobuf model = ProtobufLoader.loadFromFile(f.getAbsolutePath());
			String proxyCode = generator.generateEntireProxy(model).toString();
			String stubCode = generator.generateEntireStub(model).toString();
			
			String relativePath = generator.getProtoPath(model);
			String protoService = generator.getServices(model).iterator().next().getName();
			
			File genPath = new File(destPath.getAbsolutePath() + File.separatorChar + relativePath + File.separatorChar);
			if(!genPath.exists()) {
				genPath.mkdirs();
			}
			
			try {
				writeToFile(proxyCode, genPath, protoService + "Proxy.java");
				writeToFile(stubCode, genPath, protoService + "Stub.java");				
			}
			catch(IOException e) {
				e.printStackTrace();
				return false;
			}
		}		
		
		return true;
	}
	
	public void writeToFile(String code, File path, String filename) throws IOException {
		
		if(!path.exists()) {
			path.mkdir();
		}
		
		FileWriter fw;
		fw = new FileWriter(new File(path.getAbsolutePath()+File.separatorChar+filename));
		fw.write(code);
		fw.close();
				
	}
	
}
