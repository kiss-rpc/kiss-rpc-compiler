package org.systemticks.protobuf.rpc.compiler;

public class KissRpcCompilePluginExtension {

	private String sourcePath = "";
	private String destinationPath = "";
	
	public String getSourcePath() {
		return sourcePath;
	}
	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}
	public String getDestinationPath() {
		return destinationPath;
	}
	public void setDestinationPath(String destinationPath) {
		this.destinationPath = destinationPath;
	}
	
	
}
