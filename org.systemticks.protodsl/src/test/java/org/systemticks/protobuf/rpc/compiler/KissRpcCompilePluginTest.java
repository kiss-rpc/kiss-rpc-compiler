package org.systemticks.protobuf.rpc.compiler;

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.gradle.testkit.runner.BuildResult;
import org.gradle.testkit.runner.GradleRunner;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class KissRpcCompilePluginTest {

	@Rule
	public final TemporaryFolder testProjectDir = new TemporaryFolder();

	@Test
	public void test() throws Exception {
		setUpTestProject();
		
		BuildResult result = GradleRunner.create()
		        .withProjectDir(testProjectDir.getRoot())
		        .withPluginClasspath()
		        .withArguments(KissRpcCompilePlugin.TASK_NAME, "--stacktrace")
		        .build();

		    assertThat(
		      result.task(":" + KissRpcCompilePlugin.TASK_NAME).getOutcome(),
		         equalTo(SUCCESS));		
	}

	private void setUpTestProject() throws Exception {
		File buildFile = testProjectDir.newFile("build.gradle");
		writeFile(buildFile, "plugins { id 'kiss-rpc-compile' }");
	}

	private void writeFile(File destination, String content) throws IOException {
		BufferedWriter output = null;
		try {
			output = new BufferedWriter(new FileWriter(destination));
			output.write(content);
		} finally {
			if (output != null) {
				output.close();
			}
		}
	}
}
