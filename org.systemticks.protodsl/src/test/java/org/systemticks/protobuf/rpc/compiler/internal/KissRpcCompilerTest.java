package org.systemticks.protobuf.rpc.compiler.internal;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class KissRpcCompilerTest {

	private static File sourceDir;
	private static File destDir;

	@BeforeClass
	public static void setup() {
		sourceDir = new File("./src/test/resources");
		destDir = new File("generated");
	}

	@Test
	public void test() {
		KissRpcCompiler compiler = new KissRpcCompiler(sourceDir, destDir);

		assertTrue(compiler.doGenerate());
	}

	private static boolean deleteDirectory(File dir) {
		File[] allFiles = dir.listFiles();
		if (allFiles != null) {
			for (File file : allFiles) {
				deleteDirectory(file);
			}
		}
		return dir.delete();
	}

	@AfterClass
	public static void tearDown() {
		deleteDirectory(destDir);
	}

}
