package org.systemticks.protobuf.rpc.compiler;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.BeforeClass;
import org.junit.Test;
import org.systemticks.protobuf.rpc.compiler.internal.ProxyAndStubGenerator;

import com.google.eclipse.protobuf.ProtobufLoader;
import com.google.eclipse.protobuf.proto.Protobuf;
import com.google.eclipse.protobuf.proto.Rpc;
import com.google.eclipse.protobuf.proto.Service;

public class ProtobufModelLoaderTest {

	private static Protobuf model;
	private static ProxyAndStubGenerator generator;

	@BeforeClass
	public static void setup() {
		model = ProtobufLoader.loadFromFile("./src/test/resources/rpcdemo.proto");
		generator = new ProxyAndStubGenerator();
	}
	
	@Test
	public void testLoadProtobufModel() {		
		assertNotNull(model);		
	}

	private int toSize(Iterable<?> it) {
	    int counter = 0;
	    for (Object i : it) {
	        counter++;
	    }
	    return counter;		
	}
	
		
	@Test
	public void testGetServices() {
		Iterable<Service> services = generator.getServices(model);		
		assertEquals(1, toSize(services));		
	}

	@Test
	public void testGetRpcs() {
		Iterable<Service> services = generator.getServices(model);		
		Iterable<Rpc> rpcs = generator.getRpcs(services.iterator().next());
		assertEquals(1, toSize(rpcs));		
	}
	
	@Test
	public void testGetRequestRpc() {
		Iterable<Service> services = generator.getServices(model);		
		Iterable<Rpc> rpcs = generator.getRpcs(services.iterator().next());		
		assertTrue("pingRequest".equals(generator.request(rpcs.iterator().next())));		
	}
	
	
}
